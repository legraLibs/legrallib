#!/bin/bash
# fichier contenant des modeles. N'est pas destiné a être executé.

# FctSimple()
function FctSimple() {
    fctIn "$*"

    fctOut; return 0;
}

# FctComplete( '' '' '' ['facultatif'=valeur_par_defaut])
function FctComplete() {
    if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
    fctIn "$*"
    if [[ $IS_ROOT -eq 0 ]];then erreursAddEcho "$FUNCNAME($*) doit être lancé en root!";fctOut; return $E_NOT_ROOT; fi

    # erreur sur le nombre d'argument
    if [[ $# -ne 0 ]];      then erreursAddEcho "${BASH_SOURCE}:$FUNCNAME('arg1' 'arg2' )";       fctOut; return $E_ARG_REQUIRE;fi
    if [[ $# -ne 0 ]];      then erreursAddEcho "${BASH_SOURCE}:$FUNCNAME($#/n: 'arg1' 'arg2' ) ($*)";       fctOut; return $E_ARG_REQUIRE;fi
    if [[ $# -lt 2 ]];      then erreursAddEcho "${BASH_SOURCE}:$FUNCNAME($*): Appellé par ${FUNCNAME[1]}()";       fctOut; return $E_ARG_REQUIRE;fi

    # errreur sur un des arguments
    if [[ $# -ne 0 ]];      then erreursAddEcho "${BASH_SOURCE}:$FUNCNAME($#/n: 'arg1' 'arg2' ) ($*)";       fctOut; return $E_ARG_BAD;fi

    fctOut; return 0;
}


