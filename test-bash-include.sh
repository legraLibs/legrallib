#!/bin/bash
# Fichier (tuto) de test pour le langage bash (fichier inclua avec 'source ..')

set -u
#set -eE  # same as: `set -o errexit -o errtrace`

#####################
## APPEL DES TESTS ##
#####################
function _sourceBash(){
    fctIn "$FUNCNAME($*)"
    cheminsRelatifs
    echo ''
    displayVar '$0' "$0"
    displayVar '${BASH_SOURCE[*]}' "${BASH_SOURCE[*]}"
    displayVar '${BASH_SOURCE}' "${BASH_SOURCE}" # '/ABS/test-bash-include.sh'
    #displayVar '${BASH_SOURCE[0]}' "${BASH_SOURCE[0]}"
    displayVar '${FUNCNAME[*]}' "${FUNCNAME[*]}"
    displayVar '${FUNCNAME[0]}' "${FUNCNAME[0]}"
    

    fctOut; return 0;
}

